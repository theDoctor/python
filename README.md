Some disparate Python scripts doing various things mostly connected to my work.
When using the ORCA electronic structure program suite they come in handy for
plotting the results of mapspc and analyzing and editing PDB files for use
in ORCA's QM/MM module. This may or may not be useful in conjunction with 
other software.
